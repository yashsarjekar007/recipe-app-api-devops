variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "yash.sarjekar@outlook.com"
}

variable "db_username" {
  description = "Username for RDS postgress instance"
}

variable "db_password" {
  description = "Password for RDS postgress instance"
}